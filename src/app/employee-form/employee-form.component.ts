import { Component, OnInit, ViewChild } from '@angular/core';
import { Employees } from '../Employees';
import { EmployeeServiceService } from '../employee-service.service';
import { MatTableDataSource } from '@angular/material';
import { MatTable } from '@angular/material';
import { DataSource } from '@angular/cdk/table';
import {MatDialog, MatDialogConfig} from '@angular/material';




@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.css']
})


export class EmployeeFormComponent implements OnInit {
  public user: any;
  public employeeArray = [];
  dataSource = new MatTableDataSource(this.employeeArray);
  public formObject;
  public submitted = false;
  employeeDesignation: string;
  employeeName: string;
  employeeCompany: string;
  employeeEmail: string;
  employeecontact: string;
  employeeContact: any;
  localStorageValue: string;

  // tslint:disable-next-line: member-ordering
  displayedColumns: string[] = ['name', 'company', 'email', 'contact', 'designaton', 'actions'];


  constructor() { }


  @ViewChild(MatTable, { static: true }) table: MatTable<any>;

  onSubmit(form: any, e: any) {
    e.preventDefault();
    this.submitted = true; // For manipulation of table and form
    this.employeeArray.push(this.pushToLocalStorage(form.value)); // Local storage values are pushed to an array
    this.renderTheNewRows(this.employeeArray); // the array is pushed to mat-Table
    form.reset(); // reset the form
  }

  pushToLocalStorage(myValue) {
    // function to push to local storage
    localStorage.setItem('data', JSON.stringify(myValue));
    this.localStorageValue = localStorage.getItem('data');
    return JSON.parse(this.localStorageValue);

  }

  renderTheNewRows(a) {
    // fnction to write the Table
    this.dataSource = a;
    this.table.renderRows();
  }

  captureTheRowAndReloadTheForm(row) {
    // function to capture the row and Reload the form
    console.log(row);
    this.submitted = false;
    this.employeeName = row.name;
    this.employeeCompany = row.company;
    this.employeeEmail = row.email;
    this.employeeContact = row.contact;
    this.employeeDesignation = row.designation;

  }

  openDialog(row) {
    if (confirm('Delete the row you clicked on?')) {
      this.deleteTheRow(row);
    } else {
        return;
    }

  }




  deleteTheRow(row) {
    // Function to delete the row when row is double clicked
    console.log(row);
    this.employeeArray.splice(this.employeeArray.indexOf(row), 1);
    this.renderTheNewRows(this.employeeArray);
    // If all the rows are deleted, go back to form
    if (this.employeeArray.length === 0) {
      this.submitted = false;
      return;
    }
  }

  numberOnly(event): boolean {
    // can't press anything rather than Phone Number
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  ngOnInit() {

  }

}
