export class Employees {
    constructor(public id: number,
                public name: string,
                public company: string,
                public email: string,
                public contact: string,
                public designation: string) { }
}
